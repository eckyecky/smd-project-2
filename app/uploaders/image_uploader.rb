# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base
	
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "#{Rails.root}/uploads/#{model.class.to_s.underscore}/#{mounted_as}"
  end

	def extension_white_list
		%w(jpg jpeg gif png)
	end
	version :thumb do
		process resize_to_fit: [100,100]
	end

	def md5
		chunk=model.send(mounted_as)
		@md5||=Digest::MD5.hexdigest(chunk.read.to_s)
	end
  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
		@name ||="#{md5}.#{file.extension.downcase}" if original_filename.present?
	end

end
