class Permission < ActiveRecord::Base
	belongs_to :image
	has_one :user
	validates :level, presence:true
end
