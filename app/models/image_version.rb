class ImageVersion < ActiveRecord::Base
  belongs_to :image
  mount_uploader :store, ImageUploader
  validates :store, presence: true
  skip_callback :commit, :after, :remove_store!
  def ext
  	File.extname self.store_url
  end
end
