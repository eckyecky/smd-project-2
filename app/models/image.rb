class Image < ActiveRecord::Base
  belongs_to :user
  has_many :image_versions, dependent: :destroy
  has_many :permissions, dependent: :destroy
  
  def can_view?(user)
  	self.permissions.find_by(level:2) || (user && self.user==user || self.permissions.find_by(level:1,user_id:user.id))
  end
  def path
  	"/uploads/#{self.id}#{self.image_versions.last.ext}"
  end
  def thumb
  	"/uploads/#{self.id}.thumb#{self.image_versions.last.ext}"
	end  	
  def current_version(version=nil)
  	self.image_versions.last.store_url(version)
  end
  def description
  	self.image_versions.last.description
  end
  
end
