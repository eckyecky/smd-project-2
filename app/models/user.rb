class User < ActiveRecord::Base
	attr_accessor :authToken
	before_save { self.email=email.downcase}
	validates :firstName, presence: true
	VALID_EMAIL_PATTERN = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, format: {with:VALID_EMAIL_PATTERN}, uniqueness:{case_sensitive:false}
	validates :userName, presence: true, length: {minimum: 3,maximum:20}, uniqueness: { case_sensitive: true}
	has_secure_password
	validates :password, confirmation:true, length:{ minimum:6},on: :create
	has_many :images , dependent: :destroy
	
	def User.crypt(string)
		BCrypt::Password.create(string)
	end
	def gravatar_image
		sum=Digest::MD5.hexdigest(self.email)
		"https://s.gravatar.com/avatar/#{sum}"
	end
	def User.newToken
		SecureRandom.urlsafe_base64
	end
	def rememberMe
		self.authToken=User.newToken
		update_attribute(:authCrypt,User.crypt(authToken))
	end
	def forgetMe
		update_attribute(:authCrypt,nil)
	end
	def authed?(authToken)
		if authCrypt
			BCrypt::Password.new(authCrypt).is_password?(authToken)
		else
			false
		end
	end
end
