class ImageController < ApplicationController
	skip_before_action :require_login, only: [:fetch,:show]

	def upload
			@image=Image.new
			@image_version=@image.image_versions.new
	end
	
	def upload_post
		@image=current_user.images.new()
		@image_version=@image.image_versions.new(params.require(:image_version).permit(:store,:description))
		
		perms=params.require(:permission).permit(:level)
		level=perms[:level].to_i
		if level && level == 0
			@image.permissions.destroy_all
		elsif level && level==1
			params[:permission][:users].split(',').each do |userToken|
				userToken=userToken.strip
				user=User.find_by(userName:userToken.strip)
				user||=User.find_by(email:userToken)
				if user
					perm=@image.permissions.new()
					perm.user_id=user.id
					perm.level=1
				end
				perm.save
			end
		elsif level && level==2
			perm=@image.permissions.find_by_level(2)
			@image.permissions.create(level:2) if perm.nil?
		end

		if @image.save
			redirect_to "/image/#{@image.id}"
		else
			redirect_to '/upload'
		end
	end
	
	def fetch
		image=Image.find(params[:id])
		if image.can_view?(current_user)
			send_file(image.current_version(params[:size]),disposition: 'inline')
		else
			render('statics/noperm')
		end
	end
	
	def show		
		@image=Image.find_by_id(params[:id])
		
		if !(@image && @image.can_view?(current_user))
			render('statics/noperm')
		end
	end
	
	def mine
		if params[:start]
			offset=(params[:start].to_i-1)*10
		else
			offset=0
		end
		@images=current_user.images.offset(offset).limit(10)
	end
	def shared
		if params[:start]
			offset=(params[:start].to_i-1)*10
		else
			offset=0
		end
		@shared=Permission.where(user_id:current_user.id,level:1).offset(offset).limit(10)
	end
	def edit
		@image=Image.find_by_id(params[:id])
		@perm=@image.permissions.last.level if @image.permissions.last
		if !(@image && @image.user==current_user)
			redirect_to "/image/#{params[:id]}"
		end
	end
	def edit_post
		@image=Image.find_by_id(params[:id])
		
		level=params[:permission][:level].to_i || nil
		edits=params.require(:edit).permit(:rotate)
		if edits[:rotate].to_i>0
			magick=MiniMagick::Image.open(@image.current_version)
			magick.rotate edits[:rotate]
			version||=@image.image_versions.new
			version.store=File.new(magick.path)
		end
		version||=@image.image_versions.last
		version.update_attributes(params.require(:image).permit(:description))
		
		if level && level == 0
			@image.permissions.destroy_all
		elsif level && level==1
			params[:permission][:users].split(',').each do |userToken|
				userToken=userToken.strip
				user=User.find_by(userName:userToken.strip)
				user||=User.find_by(email:userToken)
				if user
					perm=@image.permissions.new()
					perm.user_id=user.id
					perm.level=1
				end
				perm.save
			end
		elsif level && level==2
			perm=@image.permissions.find_by_level(2)
			@image.permissions.create(level:2) if perm.nil?
		end
		
		if @image.save
			flash[:success]="Image updated"
			redirect_to "/image/#{@image.id}"
		else
			render 'upload'
		end
	end
	def delete
		@image=Image.find_by_id(params[:id])
		if @image && @image.user=current_user
			@image.destroy
			flash[:success]="Image deleted"
		end
		redirect_to '/photos'
	end
end
