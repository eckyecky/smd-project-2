class AuthenticationController < ApplicationController
	skip_before_action :require_login, except: [:profile,:edit]
	#register user
  def register
  	@user=User.new
  end

	#save new user
  def create
  	@user=User.new(user_params)
  	if @user.save
  		login_user @user
  		flash[:success] = "Thanks for joining #{@user.firstName}!"
  		redirect_to "/user/#{@user.userName}"
  	else
  		render 'register'
  	end
  end

#display user profile
  def profile
  	@user=User.find_by_userName(params[:userName])
  	if @user.nil?
  		render 'nouser'
  	end
  end

#login/display form
  def login
  	if params[:session]
  		(user=User.find_by_email(params[:session][:token].downcase) || user=User.find_by_userName(params[:session][:token]))
  		if user && user.authenticate(params[:session][:password])
				login_user user
				flash[:success]= "Welcome back #{user.firstName}!"
	  		redirect_to "/user/#{user.userName}"
  		else
  			flash.now[:danger] = 'Invalid email/password combination' # Not quite right!
  			render 'login'
  		end
  	end
  end
  
#logout
	def destroy
		logout
		redirect_to root_url  
	end

#edit user
	def edit
		@user=current_user
	end
# commit edit
	def update
		@user=current_user
		password=params[:user][:current_password]
		if password && @user.authenticate(password)
			if @user.update_attributes(update_params)
				flash[:success] = "Details updated"
				redirect_to "/user/#{@user.userName}"
			else
				render 'edit'
			end
		else
		#current_user.update_attributes
			@user.errors.add(:current_password,'incorrect!')
			render 'edit'
		end
	end
	
	private
  def user_params
  	params.require(:user).permit(:firstName,:lastName,:email,:userName,:password,:password_confirmation)
  end
  def update_params
  	params.require(:user).permit(:firstName,:lastName,:email,:userName,:password,:password_confirmation)
  end

end
