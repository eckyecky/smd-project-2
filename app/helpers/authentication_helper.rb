module AuthenticationHelper

	def login_user(user)
		remember user
		session[:user_id] = user.id
	end
	def remember(user)
		user.rememberMe
		cookies.permanent.signed[:user_id] = user.id
		cookies.permanent[:authToken] = user.authToken
	end
	def logout
		current_user.forgetMe
		cookies.delete(:user_id)
		cookies.delete(:authToken)
		session.delete(:user_id)
		@current_user=nil
	end
	def current_user
		if (user_id=session[:user_id])
			@current_user ||=User.find_by_id session[:user_id]
		elsif (user_id=cookies.signed[:user_id])
			user=User.find_by_id(user_id)
			if user && user.authed?(cookies[:authToken])
				login_user user
				@current_user = user
			end
		end
	end
	def logged_in?
		!current_user.nil?
	end
	def edit_profile_link(user)
		if logged_in? && user==current_user
			link_to "edit profile", "/user/#{user.userName}/edit"
		end
	end	
end
