module ImageHelper

	def prev_link
		page=1
		if params[:start] && params[:start].to_i>1
			page+=params[:start].to_i/10
			link_to "<<Previous" , "/photos/#{page}"
		end
	end
	def next_link
		if page=params[:start]
			page=page.to_i
		else
			page=1
		end
		render text:'<pre>'.html_safe
		render text:"#{page*10}\n"
		render text:"#{current_user.images.count}"
		render text:'</pre>'.html_safe
		if page*10<current_user.images.count
			link_to "next>>" , "/photos/#{page+1}"
		end
	end
	def img_link(image,options=nil)
		if options[:size]
			path=image.thumb
		else
			path=image.path
		end
		link_to image_tag(path), "/image/#{image.id}"
	end
end
