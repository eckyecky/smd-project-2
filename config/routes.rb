Rails.application.routes.draw do
	root 'statics#home'
	match '/register', to: 'authentication#register', via: 'get'
	match '/register', to: 'authentication#create', via: 'post'
	match '/login', to: 'authentication#login', via: ['get','post']
	match '/logout', to: 'authentication#destroy', via: 'delete'
	match '/upload', to: 'image#upload', via: 'get'	
	match '/image/edit/:id', to: 'image#edit', via: 'get'	
	match '/image/edit/:id', to: 'image#edit_post', via: 'patch'
	match '/upload', to: 'image#upload_post', via: 'post'	
	match '/uploads/:id.(:size.):ext', to: 'image#fetch', via: 'get'
	match '/user/:userName/edit', to: 'authentication#edit', via: 'get', as: 'user_edit'
	match '/user/:userName/edit', to: 'authentication#update', via: 'patch'
	match '/image/:id', to: 'image#show', via: 'get'	
	match '/shared(/:start)',to:'image#shared', via:'get'
	match '/image/delete/:id', to: 'image#delete', via: 'delete'
	match '/photos(/:start)', to: 'image#mine', via: 'get'
	
	match '/user/:userName', to: 'authentication#profile', via: 'get'

end
