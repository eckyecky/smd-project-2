class CreateImageVersions < ActiveRecord::Migration
  def change
    create_table :image_versions do |t|
      t.string :store
			t.string :description
						
      t.timestamps
    end
    add_reference :image_versions, :image, index:true
  end
end
