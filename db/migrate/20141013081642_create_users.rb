class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstName
      t.string :lastName
      t.string :email
      t.string :userName
			t.string :password_digest
			t.string :authCrypt
			
      t.timestamps
    end
  	add_index :users, :userName, {unique: true}
  	add_index :users, :email, {unique: true}
  	add_index :users, :authCrypt, {unique: true}
  end
end
