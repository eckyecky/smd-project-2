class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
			t.integer :level
      t.timestamps
    end
    add_reference :permissions, :image
    add_reference :permissions, :user
  end
end
